import Vue from 'vue';
import VueRouter from 'vue-router';
import cmsHippoPage from './components/hippoCore/CmsPage.vue';
import cmsUrls from './utils/cms-urls.js';

Vue.use(VueRouter);
const previewBasePath = `/${cmsUrls.preview.contextPath}/${cmsUrls.preview.previewPrefix}`;
const isPreview = !!(window && window.location.pathname.startsWith(previewBasePath));

let routerConfig = {
  mode: 'history',
  routes: [
    {
      path: '*',
      name: 'cmsPage',
      component: cmsHippoPage
    }
  ]
};

if (isPreview) {
  routerConfig.base = previewBasePath;
}
const router = new VueRouter(routerConfig);
  
export default router;
