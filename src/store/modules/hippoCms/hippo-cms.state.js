export default {
  path: '',
  hydrated: false,
  pageModel: {},
  previewMode: false,
  bodyComments: false,
  error: null,
}; 