import mutations from './hippo-cms.mutations';
import actions from './hippo-cms.actions';
import state from './hippo-cms.state';
import getters from './hippo-cms.getters';

export default {
  namespaced: true,
  state,
  actions,
  getters,
  mutations,
};
