export default {
  setPath(state, path) {
    state.path = path;
  },

  setHydrated(state, hydrated) {
    state.hydrated = hydrated;
  },

  setPageModel(state, pageModel) {
    state.pageModel = pageModel;
  },

  setPreviewMode(state, previewMode) {
    state.previewMode = previewMode;
  },

  addedBodyComments(state, bodyComments) {
    state.bodyComments = bodyComments;
  },

  error(state, error) {
    state.error = error;
  }
};

