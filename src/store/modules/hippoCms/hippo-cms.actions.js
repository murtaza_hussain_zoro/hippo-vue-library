import { fetchCmsPage } from '@/utils/fetch';
import { updateCmsUrls } from '@/utils/cms-urls';
import { parseRequest } from '@/utils/parse-request';

const cmsUrls = {
  preview: {
    hostname: 'cms.dev.local',
    contextPath: 'site',
    port: 80,
  }
};  

const request = { hostname: window.location.hostname, path: window.location.pathname };

export default {
  fetchPageModel: async ({ commit }, path) => {
    try {
 
      const updatedUrls = updateCmsUrls(cmsUrls);

      const parsedRequest = parseRequest(request, updatedUrls);  

      const pageModel = await fetchCmsPage(path.replace(/^\//, ''), parsedRequest.preview, updatedUrls);
      
      commit('setPath', path.replace(/^\//, ''));
      commit('setHydrated', true);
      commit('setPageModel', pageModel);
      commit('setPreviewMode', parsedRequest.preview);
             
    } catch (e) {
      commit('error', true);
    }
  }, 

  setPageModel({ commit }, pageModel){
    commit('setPageModel', pageModel);
  }, 
};  