import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

import hippoCms from './modules/hippoCms/hippo-cms.module.js';

const modules = { 
  hippoCms,
}

const store = {
  modules,
};

export default new Vuex.Store(store);

