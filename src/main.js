import Vue from 'vue';
import App from './App.vue';
import store from './store/store';
import router from './router';
import { addBeginComment, addEndComment } from '@/utils/add-html-comment';

Vue.config.productionTip = false;
Vue.config.devtools = true

Vue.directive('cms-meta', {
  inserted: function (el, bindings) {
    addBeginComment(el, bindings.value.begin, bindings.value.item, bindings.value.previewMode);
    addEndComment(el, bindings.value.end, bindings.value.item, bindings.value.previewMode);
  }
});

Vue.directive('cms-edit-button', { 
  inserted: function (el, bindings) {  
    addBeginComment(el, 'afterbegin', bindings.value.content, bindings.value.previewMode);
  }
});  

window.top.V = new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app');
       
