
const Banner = () => import(/* webpackChunkName: "cms_components" */ '@/components/static/Banner');
const NewsList = () => import(/* webpackChunkName: "cms_components" */ '@/components/static/NewsList');
const NewsItem = () => import(/* webpackChunkName: "cms_components" */ '@/components/static/NewsItem');
const CmsUndefined = () => import(/* webpackChunkName: "cms_components" */ '@/components/hippoCore/CmsUndefined');

const COMPONENT_MAPPING = {
  'banner': { component: Banner, contentComponent: true },
  'news-list': { component: NewsList },
  'news-item': { component: NewsItem, contentComponent: true },
  'cms-undefined': { component: CmsUndefined, contentComponent: true },
};

function mapComponent(label) {
  if(label) {
    const componentName = label.replace(/\s+/g, '-').toLowerCase();
    
    return COMPONENT_MAPPING[componentName];
  }
  return COMPONENT_MAPPING['cms-undefined'];
}

export { mapComponent, COMPONENT_MAPPING }; 