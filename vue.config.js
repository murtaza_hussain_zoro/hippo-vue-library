module.exports = { 
  devServer: {
    disableHostCheck: true
  },
  chainWebpack: config => {
    config.optimization.delete('splitChunks')
  },
  // configureWebpack: {
  //   publicPath: "http://spa.dev.local/" 
  // }
} 